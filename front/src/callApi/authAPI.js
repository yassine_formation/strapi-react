import { url_auth } from "./urlRoute";
import axios from 'axios'
import jwtDecode from 'jwt-decode'

function authenticate (credentials) {
    axios.post(url_auth, credentials)
    .then(res => res.data)
    .then(data => {
        window.localStorage.setItem('authToken', data.jwt)
        window.localStorage.setItem('username', data.user.username)
        axios.defaults.headers["Authorization"] = 'Bearer' + data.jwt
        console.log(isAuthenticated())
    })
}

function isAuthenticated(){
    const token = window.localStorage.getItem('authToken')

    if(token) {
        const {exp} = jwtDecode(token)
        if(exp * 1000 > new Date().getTime()){
            return true
        }
        return false
    }
    return false
}
const auth = {
    authenticate,
    isAuthenticated
}

export default auth
