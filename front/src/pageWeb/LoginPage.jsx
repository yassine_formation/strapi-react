import React, {useContext, useState} from "react";
import { Link, useNavigate } from "react-router-dom";
import auth from "../callApi/authAPI";
import authContex from "../contexts/authContext";

function LoginPage(){
    const [credentials, setCredentials] = useState({
        identifier : "",
        password : ""
    })
    const navigate = useNavigate()
    const {setIsAuthenticated} = useContext(authContex)

    const handleChange = ({currentTarget}) => {
        const {value, name} = currentTarget
        setCredentials({
            ...credentials,
            [name]: value

        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        try {
            await auth.authenticate(credentials)
            setIsAuthenticated(true)
            navigate('/admin')
            
        }
        catch(error){
            console.log(error)
        }
    }

    return(
        <div>
            <form onSubmit={handleSubmit}>
                <div>
                    <label htmlFor="identifier">Identifier</label>
                    <input type="text" name="identifier" id="identifier" onChange={handleChange} />
                </div>
                <div>
                    <label htmlFor="password">Password</label>
                    <input type="password" name="password" id="password" onChange={handleChange} />
                </div>
                <div>
                    <input type="submit" value="submit" />
                    <input type="reset" value="reset" />
                    <Link to = '/register'>s'incrire</Link>
                </div>
            </form>
        </div>
    )
}
export default LoginPage