import React, { useState } from 'react';
import axios from 'axios';
import { Link, useNavigate } from 'react-router-dom';

const Register = () => {
  const [newUser, setNewUser] = useState({
    username: '',
    email: '',
    password: '',
  });
  const navigate = useNavigate();

  const handleRegister = async (event) => {
    event.preventDefault();
    try {
      const response = await axios.post('http://localhost:1337/api/auth/local/register', {
        username: newUser.username,
        email: newUser.email,
        password: newUser.password,
      });
      console.log(response.data);
      alert('Utilisateur enregistré !');
      navigate('/')
    } catch (error) {
      console.error(error);
      alert("Une erreur s'est produite lors de l'enregistrement de l'utilisateur !");
    }
  };

  const handleChange = (event) => {
    const { name, value } = event.target;
    setNewUser((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  return (
    <div>
      <h1>Inscription</h1>
      <form onSubmit={handleRegister}>
        <label htmlFor="username">
          Nom d'utilisateur :
          <input
            type="text"
            id="username"
            name="username"
            value={newUser.username}
            onChange={handleChange}
          />
        </label>
        <label htmlFor="email">
          Email :
          <input
            type="email"
            id="email"
            name="email"
            value={newUser.email}
            onChange={handleChange}
          />
        </label>
        <label htmlFor="password">
          Mot de passe :
          <input
            type="password"
            id="password"
            name="password"
            value={newUser.password}
            onChange={handleChange}
          />
        </label>
        <button type="submit">S'inscrire</button>
        <Link to='/'>se connecter</Link>
      </form>
    </div>
  );
};

export default Register;
