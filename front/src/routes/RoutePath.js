import React, { useState } from "react";
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import LoginPage from "../pageWeb/LoginPage";
import RegisterPage from "../pageWeb/RegisterPage";
import AdminPage from "../pageWeb/AdminPage";
import AuthContex from "../contexts/authContext";
import auth from "../callApi/authAPI";
import PrivateRoute from "./PrivateRoute";

function RoutePath(){
    const [isAuthenticated, setIsAuthenticated] = useState(auth.isAuthenticated)
    return (
        <AuthContex.Provider
        value={
            {isAuthenticated,
            setIsAuthenticated}
        }>
            <Router>
                <Routes>
                <Route path="/" element={<LoginPage></LoginPage>}></Route>
                <Route path="/register" element={<RegisterPage></RegisterPage>}></Route>
                <Route path='/admin/*' element={<PrivateRoute  element={<AdminPage />} />}/>
                </Routes>
        </Router>
        </AuthContex.Provider>
        
    )
}
export default RoutePath;