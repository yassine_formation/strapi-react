import React, { useContext } from "react";
import authContex from "../contexts/authContext";
import { useNavigate, Navigate, Route, Routes } from "react-router-dom";





const PrivateRoute = ({ path, element }) => {
    const { isAuthenticated } = useContext(authContex);
  
    if (isAuthenticated) {
      return <Routes><Route path="/adminPage" element={element} /></Routes>;
    }
  
    return <Navigate to="/register" />;
  };
  export default PrivateRoute